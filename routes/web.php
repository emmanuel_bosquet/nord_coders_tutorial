<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// declare a route for a GET method
Route::get(
    // on the root uri
    '/',
    // and please call the index method of the PostController
    [PostController::class, 'index']

    // to call the route dynamically
)->name('welcome');

Route::get('/posts/{id}', [PostController::class, 'show'])->name('posts.show');
// Route::get('posts', function () {
//     return response()->json([
//         'title' => 'mon super titre',
//         'description' => 'ma super description'
//     ]);
// });

// Route::get('articles', function () {
//     return view('articles');
// });
