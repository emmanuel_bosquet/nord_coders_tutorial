<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::all();

        // dump the data into readable json-like stuff
        dd($posts);

        return view('articles', ['posts' => $posts]);
    }

    public function show($id)
    {

        $post = Post::find($id);

        // dd($post);

        return view('article', ['post' => $post]);
    }
}
