<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Articles</title>
</head>

<body>

    <h1>Liste des articles</h1>
    @if ($posts->count() > 0)
        @foreach ($posts as $post)
            <h3>
                {{-- this calls a route dynamically --}}
                <a href="{{ route('posts.show', ['id' => $post->id]) }}">
                    {{ $post->title }}
                </a>
            </h3>
            <p>{{ $post->content }}</p>
        @endforeach
    @else
        <span>Aucun post en base de données</span>
    @endif

</body>

</html>
