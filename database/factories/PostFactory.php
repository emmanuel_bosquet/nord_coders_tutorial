<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence,
            // this looks like a function but really it is a property (attribute?)
            'content' => $this->faker->paragraph,
            'created_at' => now(),


        ];
    }
}
